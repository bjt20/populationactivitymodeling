load PaperData\Figure2\AveMagnitudesOfMomentPerInput.mat

fname='PaperData\Figure2\pathways_anatomy_factsheets_simplified.json';
fid = fopen(fname);
raw = fread(fid,inf);
str = char(raw');
fclose(fid);
val = jsondecode(str);

fname='PaperData\Figure2\mtype_map.txt';
raw = readtable(fname);
neurons=table2cell(raw(:,2));
Connections=zeros(55,1);
Synapses=zeros(55,1);
totalSynapses=0;
for i=1:length(neurons)
    for j=1:length(neurons)
        if isfield(val,[neurons{i} '_' neurons{j}])
            connection=val.([neurons{i} '_' neurons{j}]);
            Connections(i,j)=connection.number_of_divergent_neuron_mean;
            Synapses(i,j)=connection.mean_number_of_synapse_per_connection;
            totalSynapses=totalSynapses+connection.total_synapse_count;
        end
    end
end
%% Num neurons per 100nA-m signal for diff windows
% start=[400 400 400 1];
% stop=[800 1200 2400 4000];
window=800:4000;
numneurons=zeros(length(window),55,5);
for i=1:length(window)
    numneurons(i,:,:)=1e8./abs(mean(aveMags(400:window(i),:,:)));
end
%%
which=[2 7 25 29 43];
WhichConnections=Connections(:,which)';
data=squeeze(numneurons(:,29,4))./WhichConnections(4,29);
figure
plot((window-400)./40,data,'LineWidth',2)

load PaperData\Figure2\DipoleMags.mat
ct=1;
averages=cell(5,5);
windowSizes=400:1:3600;
for i=1:5
    for j=1:5
        for k=1:length(windowSizes)
            averages{i,j}=[averages{i,j} mean(dipoles{ct}(1:windowSizes(k),3)-dipoles{ct}(1,3)).*1000];%fA-m
        end
        ct=ct+1;
    end
end

colors=parula(5);
% figure
% hold on
hold on
data2=mean(abs(100000000./vertcat(averages{4,:})));
plot(windowSizes./40,data2,'LineWidth',2)
axis([10 90 1e5 2e6])
xlabel('Window (ms)')
% set(gca, 'YScale', 'log')
ylabel('Number of Neurons')
fileName=['Figures\CompNumNeuronsTo100nAmVsWindowSize_' num2str(4) '.pdf'];
% print(gcf,fileName,'-bestfit','-dpdf');

%%
load PaperData\Figure2\NumPSCsPer.mat

fname='PaperData\Figure2\pathways_anatomy_factsheets_simplified.json';
fid = fopen(fname);
raw = fread(fid,inf);
str = char(raw');
fclose(fid);
val = jsondecode(str);

fname='PaperData\Figure2\mtype_map.txt';
raw = readtable(fname);
neurons=table2cell(raw(:,2));
Connections2=zeros(55,1);
Synapses=zeros(55,1);
totalSynapses=0;
for i=1:length(neurons)
    for j=1:length(neurons)
        if isfield(val,[neurons{i} '_' neurons{j}])
            connection=val.([neurons{i} '_' neurons{j}]);
            Connections2(i,j)=connection.number_of_convergent_neuron_mean;
            Synapses(i,j)=connection.mean_number_of_synapse_per_connection;
            totalSynapses=totalSynapses+connection.total_synapse_count;
        end
    end
end
%%
figure
plot(windowSizes./40,1./data.*2./(1./data+1./data2'.*3.3).*100,'LineWidth',2)
hold on
plot(windowSizes./40,1./data2'./(1./data+1./data2'.*3.3).*100,'LineWidth',2)
axis([10 90 0 100])
xlabel('Window (ms)')
ylabel('Relative % Contribution to EEG signal')
legend('L5-L5 EPSC Connections','APs')
fileName=['Figures\PercentContributionToEEG_' num2str(4) '.pdf'];
% print(gcf,fileName,'-bestfit','-dpdf');

%% Num neurons per 100nA-m signal for diff windows
% start=[400 400 400 1];
% stop=[800 1200 2400 4000];
window=800:4000;
numneurons=zeros(length(window),55,5);
for i=1:length(window)
    numneurons(i,:,:)=1e8./abs(mean(aveMags(400:window(i),:,:)));
end
%%
% colors=parula(55);
load PaperData\Figure2\Colormap.mat
titles={'L1 NGC','L2/3 PC','L4 LBC','L5 PC','L6 PC'};
for i=1:5
    figure
    WhichConnections=Connections2(:,which)';
    data=squeeze(numneurons(:,:,i))./WhichConnections(i,:);
    for j=1:55
        semilogy(windowSizes./40,data(:,j),'LineWidth',2,'Color',colors(j,:))
        hold on
    end
    data2=mean(abs(100000000./vertcat(averages{i,:})));
    semilogy(windowSizes./40,data2,'k-','LineWidth',4)
    axis([10 90 1e5 1e10])
    xlabel('Window (ms)','Fontsize',16)
    ylabel('# Neurons','Fontsize',16)
    title(titles{i},'Fontsize',16)
    fileName=['Figures\NumNeuronsTo100nAmToEEG_' num2str(i) '.png'];
%         print(gcf,fileName,'-bestfit','-dpdf','-painters');
%     print(fileName,'-dpng')
end
%%
for i=1:5
    WhichConnections=Connections2(:,which)';
    data=squeeze(numneurons(:,:,i))./WhichConnections(i,:);
    
%     colors=parula(55);
    figure
    hold on
    data2=mean(abs(100000000./vertcat(averages{i,:})));
    for j=1:55
        plot(windowSizes./40,1./data(:,j)./(sum(1./data,2)+1./data2').*100,'LineWidth',2,'Color',colors(j,:))
    end
    hold on
    
    plot(windowSizes./40,1./data2'./(sum(1./data,2)+1./data2').*100,'LineWidth',4,'Color','k')
    xlabel('Window (ms)')
    ylabel('Relative % Contribution to EEG signal')
    axis([10 90 0 100])
    fileName=['Figures\PercentContributionToEEG_' num2str(i) '.pdf'];
%         print(gcf,fileName,'-bestfit','-dpdf','-painters');
end

%%
data3=[];
data4=[];
figure
for i=1:5
    WhichConnections=Connections2(:,which)';
    data=squeeze(numneurons(:,:,i))./WhichConnections(i,:);
    
%     colors=parula(55);
    figure
    hold on
    data2=mean(abs(100000000./vertcat(averages{i,:})));
    
    plot(windowSizes./40,sum(1./data,2)./(sum(1./data,2)+1./data2').*100,'LineWidth',2,'Color',colors(j,:))
    hold on
    data3=[data3 sum(1./data,2)];
    plot(windowSizes./40,1./data2'./(sum(1./data,2)+1./data2').*100,'LineWidth',2,'Color','k')
    data4=[data4 1./data2'];
    xlabel('Window (ms)')
    ylabel('Relative % Contribution to EEG signal')
    axis([10 90 0 100])
    fileName=['Figures\PercentContributionToEEG_' num2str(i) 'Combined.pdf'];
%         print(gcf,fileName,'-bestfit','-dpdf','-painters');
end
%%
load PaperData\Figure2\ProportionOfNeuronTypes.mat
which=[2 7 25 29 43];
proportions=(proportionofTotal(which)'./sum(proportionofTotal(which)));
figure
plot(windowSizes./40,data3*proportions./(data3*proportions+data4*proportions).*100)% PSC
hold on
plot(windowSizes./40,data4*proportions./(data3*proportions+data4*proportions).*100)% AP
axis([10 90 0 100])
fileName=['Figures\PercentContributionToEEG_weighted.pdf'];
% print(gcf,fileName,'-bestfit','-dpdf','-painters');
%%
load PaperData\Figure2\ProportionOfNeuronTypes.mat
which=[2 7 25 29 43];
proportions=(proportionofTotal(which)'./sum(proportionofTotal(which)));
figure
hold on
% plot(windowSizes./40,data3,'k-')% PSC
data=1e8./numneurons;
colors=[0.2081 0.1663 0.5292;...
    0.635 0.075 0.184;...
    1 0 1; ...
    0 0.5 0;...
    0.5 0.1 0.098;];%blue,red,purple,green,orange
for i=1:5
    data2=squeeze(data(:,:,i))*WhichConnections(i,:)';
    plot(linspace(10,90,3201),data2,'-','Color',colors(i,:),'LineWidth',2)
end

for i=1:5
    plot(linspace(10,90,3201),abs(mean(vertcat(averages{i,:}))),'--','Color',colors(i,:),'LineWidth',2)
end
xlabel('Window(ms)')
ylabel('Signal per neuron (fA-m)')
% plot(windowSizes./40,data4,'r-')% AP
% axis([10 90 0 100])
fileName=['Figures\AbsoluteSignalPerNeuron.pdf'];
% print(gcf,fileName,'-bestfit','-dpdf','-painters');
%%
load PaperData\Figure2\ProportionOfNeuronTypes.mat
which=[2 7 25 29 43];
proportions=(proportionofTotal(which)'./sum(proportionofTotal(which)));
figure
hold on
% plot(windowSizes./40,data3,'k-')% PSC
data=1e8./numneurons;
colors=[0.2081 0.1663 0.5292;...
    0.635 0.075 0.184;...
    1 0 1; ...
    0 0.5 0;...
    0.5 0.1 0.098;];%blue,red,purple,green,orange
for i=1:5
    data2=squeeze(data(:,:,i))*WhichConnections(i,:)';
    plot(linspace(10,90,3201),data2.*proportions(i),'-','Color',colors(i,:),'LineWidth',2)
end

for i=1:5
    plot(linspace(10,90,3201),abs(mean(vertcat(averages{i,:}))).*proportions(i),'--','Color',colors(i,:),'LineWidth',2)
end
xlabel('Window(ms)')
ylabel('Signal per neuron (fA-m)')
% plot(windowSizes./40,data4,'r-')% AP
% axis([10 90 0 100])
fileName=['Figures\AbsoluteSignalPerNeuron_Scaled.pdf'];
% print(gcf,fileName,'-bestfit','-dpdf','-painters');
