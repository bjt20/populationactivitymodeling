load PaperData\Figure3\NumPSCsPer.mat

fname='PaperData\Figure3\pathways_anatomy_factsheets_simplified.json';
fid = fopen(fname);
raw = fread(fid,inf);
str = char(raw');
fclose(fid);
val = jsondecode(str);

fname='PaperData\Figure3\mtype_map.txt';
raw = readtable(fname);
neurons=table2cell(raw(:,2));
Connections=zeros(55,1);
Synapses=zeros(55,1);
totalSynapses=0;
for i=1:length(neurons)
    for j=1:length(neurons)
        if isfield(val,[neurons{i} '_' neurons{j}])
            connection=val.([neurons{i} '_' neurons{j}]);
            Connections(i,j)=connection.number_of_convergent_neuron_mean;
            Synapses(i,j)=connection.mean_number_of_synapse_per_connection;
            totalSynapses=totalSynapses+connection.total_synapse_count;
        end
    end
end
%%
figure
data=numneurons;
data(data<0)=1;
imagesc(log10(data))
colormap parula
caxis([7 10])

which=[2 7 25 29 43];
WhichConnections=Connections(:,which)';
data=numneurons./WhichConnections;
data(data<0)=inf;
data(data>1e10&data<1e23)=1e10;
figure
imagesc(log10(data))
caxis([5 10.1])
colormap(flipud([1 1 1;parula]))
colorbar
% print('-dpdf', '-painters', ['Figures\NumberOfInputsToGenerateEEG.pdf']);
load PaperData\Figure3\ProportionOfNeuronTypes.mat
which=[2 7 25 29 43];
WhichConnections=Connections(:,which)';
data=numneurons./WhichConnections./proportionofTotal;
data(data<0)=0;
data=1./data;
data(data>1)=0;
data=data.*(proportionofTotal(which)'./sum(proportionofTotal(which)));
data=data./sum(sum(data));
data=log10(data);
data(data>-20&data<-3)=-3;
figure
imagesc(data)
colormap([1 1 1;parula])
colorbar
caxis([-3.1 0])
% print('-dpdf', '-painters', ['Figures\ProportionOfSignalGeneratedByInputs.pdf']);
%%

colors=[0.2081 0.1663 0.5292; 0.0265 0.6137 0.8135;...
    0.635 0.075 0.184;1 0 0;...
    1 0 1; 0.5 0 0.5;...
    0 0.5 0; 0 1 0;...
    0.5 0.1 0.098;1 0.5 0];%blue,red,purple,green,orange
colorlabels=[ones(6,1);ones(10,1).*2;ones(12,1).*3;ones(13,1).*4;ones(14,1).*5];
numColors=[6 10 12 13 14];
load PaperData\Figure3\AveMagnitudesOfMomentPerInput.mat
figure
hold on
ct=1;
cmap=[];
for j=1:5
    for i=1:numColors(j)
        val=i/numColors(j)*colors((j-1)*2+1,:)+(1-i/numColors(j))*colors((j)*2,:);
        cmap=[cmap;val];
        plot(linspace(0,100,4000),squeeze(-aveMags(:,ct,4)),'Color',val,'LineWidth',2)
        axis([0 100 -16 4])
        ct=ct+1;
    end
end
colorbar
colormap(cmap)
% print('-dpdf', '-painters', ['Figures\ProportionOfSignalGeneratedByInputsTraces_recoloredColorbar.pdf']);


