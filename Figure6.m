%% time series reconstructions with just APs
load PaperData\Figure6\DipoleMags.mat
angles(:,1)=[];
tic
timeseries=zeros(3,100001);
maxangles=[];
maxDipoles=[];
time=1:4000;
nneurons=10000;
pop2=0;%100;
freq=100;
freq2=1;
randtime=randi(4000,nneurons,10);
probfire=10./40000+10./40000.*sin(2.*pi.*freq.*((1:length(timeseries)-4000).*0.025.*0.001));
probfire2=10./40000+10./40000.*sin(2.*pi.*freq2.*((1:length(timeseries)-4000).*0.025.*0.001));

activity=rand(nneurons,100001-4000);
activity(1:(nneurons-pop2),:)=activity(1:(nneurons-pop2),:)<probfire;
activity((nneurons-pop2+1):end,:)=activity((nneurons-pop2+1):end,:)<probfire2;

neuronType=randi(5,nneurons,1)+15;
theta=zeros(nneurons,4000);
mag=zeros(nneurons,4000);
for i=1:length(neuronType)
    theta(i,:)=angles(neuronType(i),:);
    %     if neuronType(i)>=11 & neuronType(i)<=15
    %         mag(i,:)=zeros(1,4001);
    %     else
    mag(i,:)=dipolesMag{neuronType(i)}(2:end);
    %     end
end
X=rand([nneurons 1])-0.5;
Y=rand([nneurons 1])-0.5;
dipoletime=zeros(3,4000,nneurons);
for i=1:nneurons
    Z=sqrt((X(i).^2 + Y(i).^2).*cosd(theta(i,:)).^2./(1-cosd(theta(i,:)).^2));
    dipole = [X(i).*ones(1,4000);Y(i).*ones(1,4000);Z];
    dipole = dipole./sqrt(sum(dipole.^2)).*mag(i,:);
    dipole(3,theta(i,:)>90)= -dipole(3,theta(i,:)>90);% account for the squaring effect
    %     SingleDipole=sum(dipole);
    dipoletime(:,:,i)=dipole;
end

for i=1:nneurons
    spike=find(activity(i,:)==1);

    ISI=[diff(spike) 4000];
    for j=1:length(spike)
        if ISI(j)>=4000
            timeseries(:,(spike(j):(spike(j)+3999)))=timeseries(:,(spike(j):(spike(j)+3999)))+squeeze(dipoletime(:,:,i));
        else
            timeseries(:,(spike(j):(spike(j+1)-1)))=timeseries(:,(spike(j):(spike(j+1)-1)))+squeeze(dipoletime(:,1:(spike(j+1)-spike(j)),i));
        end
    end
end
dipoleMag=sqrt(sum(timeseries.^2));
figure
plot(linspace(0,2.5,length(dipoleMag)),dipoleMag)
axis([0 2 0 1000])
xlabel('Time (s)')
ylabel('Dipole Moment pA-m')
title([num2str(freq) ' Hz'])
fileName=['Figures\TimeSeriesRecon_' num2str(freq) '.pdf'];
% print(gcf,fileName,'-bestfit','-dpdf','-painters');

figure
plot(linspace(0,2.4,length(probfire)),probfire.*1000)
axis([0 2 0 1])
xlabel('Time (s)')
ylabel('P(Spike)/neuron/ms')

fileName=['Figures\ProbFireRecon_' num2str(freq) '.pdf'];
% print(gcf,fileName,'-bestfit','-dpdf','-painters');


toc

%% Fit to sines
freqs=1:100;
load PaperData\Figure6\DipoleMags.mat
angles(:,1)=[];
tic

maxangles=[];
maxDipoles=[];
time=1:4000;
nneurons=10000;
pop2=0;%100;
amps=zeros(100,1);
intrinsicFreq=[10 20 50 100];
for intf=1:length(intrinsicFreq)
    for f=freqs
        timeseries=zeros(3,100001);
        freq=f;
        freq2=1;
        randtime=randi(4000,nneurons,10);
        probfire=intrinsicFreq(intf)./40000+intrinsicFreq(intf)./40000.*sin(2.*pi.*freq.*((1:length(timeseries)-4000).*0.025.*0.001));
            probfire2=intrinsicFreq(intf)./40000+intrinsicFreq(intf)./40000.*sin(2.*pi.*freq2.*((1:length(timeseries)-4000).*0.025.*0.001));
        activity=rand(nneurons,100001-4000);
        activity(1:(nneurons-pop2),:)=activity(1:(nneurons-pop2),:)<probfire;
        activity((nneurons-pop2+1):end,:)=activity((nneurons-pop2+1):end,:)<probfire2;
        
        neuronType=randi(5,nneurons,1)+15;
        theta=zeros(nneurons,4000);
        mag=zeros(nneurons,4000);
        for i=1:length(neuronType)
            theta(i,:)=angles(neuronType(i),:);
            %     if neuronType(i)>=11 & neuronType(i)<=15
            %         mag(i,:)=zeros(1,4001);
            %     else
            mag(i,:)=dipolesMag{neuronType(i)}(2:end);
            %     end
        end
        X=rand([nneurons 1])-0.5;
        Y=rand([nneurons 1])-0.5;
        dipoletime=zeros(3,4000,nneurons);
        for i=1:nneurons
            Z=sqrt((X(i).^2 + Y(i).^2).*cosd(theta(i,:)).^2./(1-cosd(theta(i,:)).^2));
            dipole = [X(i).*ones(1,4000);Y(i).*ones(1,4000);Z];
            dipole = dipole./sqrt(sum(dipole.^2)).*mag(i,:);
            dipole(3,theta(i,:)>90)= -dipole(3,theta(i,:)>90);% account for the squaring effect
            %     SingleDipole=sum(dipole);
            dipoletime(:,:,i)=dipole;
        end
        
        for i=1:nneurons
            spike=find(activity(i,:)==1);
            
            ISI=[diff(spike) 4000];
            for j=1:length(spike)
                if ISI(j)>=4000
                    timeseries(:,(spike(j):(spike(j)+3999)))=timeseries(:,(spike(j):(spike(j)+3999)))+squeeze(dipoletime(:,:,i));
                else
                    timeseries(:,(spike(j):(spike(j+1)-1)))=timeseries(:,(spike(j):(spike(j+1)-1)))+squeeze(dipoletime(:,1:(spike(j+1)-spike(j)),i));
                end
            end
        end
        dipoleMag=sqrt(sum(timeseries.^2));
        %     ft = fittype(['sin((x*2*pi/' num2str(1/freq) ' + xshift))*yscale+yshift'],'coefficients',{'xshift','yshift','yscale'});
        %     mdl = fit(linspace(0,2.5,length(dipoleMag))',dipoleMag',ft,'startpoint',[1 1 1],'Lower',[0 0 max(dipoleMag).*0.15],'Upper',[2*pi max(dipoleMag) max(dipoleMag).*0.5 ]);
        amps(intf,f)=max(dipoleMag(10000:90000))-min(dipoleMag(10000:90000));%mdl.yscale;
    end
end
% save PostSynapticCurrents\PopulationAmplitudes.mat amps
figure
plot(freqs,amps(1,:))
hold on
plot(freqs,amps(2,:))
plot(freqs,amps(3,:))
plot(freqs,amps(4,:))

xlabel('Frequency (Hz)')
ylabel('Amplitude_{p-p} (pA-m)')
legend('10 Hz','20 Hz','50 Hz','100 Hz')
fileName=['Figures\AmplitudePPVFreq_' num2str(freq) '.pdf'];
% print(gcf,fileName,'-bestfit','-dpdf','-painters');
toc
%%
f=5;
load PaperData\Figure6\DipoleMags.mat
angles(:,1)=[];
tic

maxangles=[];
maxDipoles=[];
time=1:4000;
nneurons=[1 2 5 10 20 50 100 200 500 1000 2000 5000 10000];
pop2=0;%100;
amps=zeros(length(nneurons),1);
intrinsicFreq=[10 20 50 100];
for intf=1:length(intrinsicFreq)
for q=1:length(nneurons)
    timeseries=zeros(3,100001);
    freq=f;
    freq2=1;
    randtime=randi(4000,nneurons(q),10);
    probfire=intrinsicFreq(intf)./40000+intrinsicFreq(intf)./40000.*sin(2.*pi.*freq.*((1:length(timeseries)-4000).*0.025.*0.001));
    probfire2=intrinsicFreq(intf)./40000+intrinsicFreq(intf)./40000.*sin(2.*pi.*freq2.*((1:length(timeseries)-4000).*0.025.*0.001));

    activity=rand(nneurons(q),100001-4000);
    activity(1:(nneurons(q)-pop2),:)=activity(1:(nneurons(q)-pop2),:)<probfire;
    activity((nneurons(q)-pop2+1):end,:)=activity((nneurons(q)-pop2+1):end,:)<probfire2;

    neuronType=randi(5,nneurons(q),1)+15;
    theta=zeros(nneurons(q),4000);
    mag=zeros(nneurons(q),4000);
    for i=1:length(neuronType)
        theta(i,:)=angles(neuronType(i),:);
        %     if neuronType(i)>=11 & neuronType(i)<=15
        %         mag(i,:)=zeros(1,4001);
        %     else
        mag(i,:)=dipolesMag{neuronType(i)}(2:end);
        %     end
    end
    X=rand([nneurons(q) 1])-0.5;
    Y=rand([nneurons(q) 1])-0.5;
    dipoletime=zeros(3,4000,nneurons(q));
    for i=1:nneurons(q)
        Z=sqrt((X(i).^2 + Y(i).^2).*cosd(theta(i,:)).^2./(1-cosd(theta(i,:)).^2));
        dipole = [X(i).*ones(1,4000);Y(i).*ones(1,4000);Z];
        dipole = dipole./sqrt(sum(dipole.^2)).*mag(i,:);
        dipole(3,theta(i,:)>90)= -dipole(3,theta(i,:)>90);% account for the squaring effect
        %     SingleDipole=sum(dipole);
        dipoletime(:,:,i)=dipole;
    end

    for i=1:nneurons(q)
        spike=find(activity(i,:)==1);

        ISI=[diff(spike) 4000];
        for j=1:length(spike)
            if ISI(j)>=4000
                timeseries(:,(spike(j):(spike(j)+3999)))=timeseries(:,(spike(j):(spike(j)+3999)))+squeeze(dipoletime(:,:,i));
            else
                timeseries(:,(spike(j):(spike(j+1)-1)))=timeseries(:,(spike(j):(spike(j+1)-1)))+squeeze(dipoletime(:,1:(spike(j+1)-spike(j)),i));
            end
        end
    end
    dipoleMag=sqrt(sum(timeseries.^2));
%     ft = fittype(['sin((x*2*pi/' num2str(1/freq) ' + xshift))*yscale+yshift'],'coefficients',{'xshift','yshift','yscale'});
%     mdl = fit(linspace(0,2.5,length(dipoleMag))',dipoleMag',ft,'startpoint',[1 1 1],'Lower',[0 0 max(dipoleMag).*0.15],'Upper',[2*pi max(dipoleMag) max(dipoleMag).*0.5 ]);
    amps(intf,q)=max(dipoleMag(10000:90000))-min(dipoleMag(10000:90000));%mdl.yscale;
end
end
figure
plot(nneurons,amps(1,:))
hold on
plot(nneurons,amps(2,:))
plot(nneurons,amps(3,:))
plot(nneurons,amps(4,:))
xlabel('# Neurons')
ylabel('Amplitude_{p-p} (pA-m)')
legend('10 Hz','20 Hz','50 Hz','100 Hz')
fileName=['Figures\AmplitudePPVnNeurons_' num2str(freq) '.pdf'];
% print(gcf,fileName,'-bestfit','-dpdf','-painters');
toc