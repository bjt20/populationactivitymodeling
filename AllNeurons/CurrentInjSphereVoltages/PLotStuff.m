figure
load DipolynessAndMomentAll_BestFit_RegBox.mat
radii=[0.1 0.5 1 2 3 4 5 6 7 8 9 10 15 ];
%%
figure
for i=1:5
    hold on
    errorbar(radii,mean(abs(dipolyness(((i-1).*5)+(1:5),:))),std(abs(dipolyness(((i-1).*5)+(1:5),:)))./sqrt(5),'LineWidth',2)
end
axis([0 10 0 1])
ax=gca;
ax.XScale='Log';
xlabel('Radii of Recording Sphere (mm)','FontSize',16)
ylabel('Correlation Coefficient (R)','FontSize',16)
title('All Neurons','FontSize',20)
legend('L1 NGC','L2/3 PC','L4 LBC','L5 PC','L6 PC')
% print(['Figures\Neuron_AllNeuronTraces_AnyDirection.png'],'-dpng')
fileName=['Figures\Neuron_AllNeuronTraces_AnyDirection.pdf'];
print(gcf,fileName,'-bestfit','-dpdf');
%%
names={'Layer 1 NGC','Layer 2/3 PC','Layer 4 LBC','Layer 5 PC','Layer 6 PC'};
for j=1:5
    figure
    for i=1:5
        hold on
        plot(radii,abs(dipolyness(((j-1).*5)+(i),:)),'LineWidth',2)
    end
    axis([0 10 0 1])
    ax=gca;
    ax.XScale='Log';
    xlabel('Radii of Recording Sphere (mm)','FontSize',16)
    ylabel('Correlation Coefficient (R)','FontSize',16)
    title(names{j},'FontSize',20)
%     print(['Figures\Neuron_' num2str(j) 'SingleNeuronTraces_AnyDirection.png'],'-dpng')
    fileName=['Figures\Neuron_' num2str(j) 'SingleNeuronTraces_AnyDirection.pdf'];
    print(gcf,fileName,'-bestfit','-dpdf');
end
%%
load DipolynessAndMomentAll_AllDir_NoAxon.mat
figure
data=directions(:,13);
data=reshape(data,[5,5]);
orig=[sind(90).*cosd(180) sind(90).*sind(180) cosd(90)];
for i=1:5
    for j=1:5
        [X,Y]=ind2sub([360 360],data(i,j));
        x=sind(X).*cosd(Y);
        y=sind(X).*sind(Y);
        z=cosd(X);
        angle=acosd(x.*orig(1)+y.*orig(2)+z.*orig(3));
        direction(i,j)=min([abs(180-angle) abs(angle)]);
    end
end
for i=1:5
    hold on
    plot([-0.1 -0.05 0 0.05 0.1]+i,direction(:,i),'.','MarkerSize',30)
end
xticks([ 1 2 3 4 5]);
xticklabels({'L1 NGC','L2/3 PC','L4 LBC','L5 PC','L6 PC'})
xlabel('Neuron Type','FontSize',16)
ylabel('Angle Difference (^o)','FontSize',16)
title('Difference in Dipole Orientation from Somatodendritic Axis','FontSize',14)
% print -dpng Figures\AngleDifference.png
fileName=['Figures\AngleDifference.pdf'];
print(gcf,fileName,'-bestfit','-dpdf');
%%
load DipolynessAndMomentAll_BestFit_RegBox.mat
figure
data=posz(:,13);
data=reshape(data,[5 5]);
for i=1:5
    hold on
    plot(abs(data(:,i)),direction(:,i),'.','MarkerSize',30)
end
xlabel('Correlation (R)','FontSize',16)
ylabel('Angle Difference (^o)','FontSize',16)
title('Difference in Dipole Orientation from Somatodendritic Axis vs Correlation','FontSize',11)
% print -dpng Figures\AngleDifferenceVSCorrelationCoeff.png
fileName=['Figures\AngleDifferenceVSCorrelationCoeff.pdf'];
print(gcf,fileName,'-bestfit','-dpdf');