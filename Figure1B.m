cd simnibs\simTBSnrn\mat
mat_dir = addPaths_simTBSnrn;
cd ../../..
i=16;
m = struct();
m.cell_model_name = cellModelNames(i,'cell_models_file','cell_models');

% cd simnibs\simTBSnrn\mat\plot_cell_data
cell_id = 16; 
nrn_model_ver = 'maxH'; % only one included in local cell_data/
% Plot settings
lw = 2; % line width
% Load cell_data: coordinates and connectivity of compartments (parent_inds)
cell_data = loadCellData(cell_id,nrn_model_ver); % use default data_files
%% PSC
figure
plotCellLines('cell_data',cell_data,'lw',lw); 

% figure
% plot(dipolesX{16}(:,1)-dipolesX{16}(1,1))
% hold on
% plot(dipolesY{16}(:,1)-dipolesY{16}(1,1))
% plot(dipolesZ{16}(:,1)-dipolesZ{16}(1,1))

angle=0:360;
dirs=[sind(angle);zeros(1,length(angle));cosd(angle)];

load('D:\PopulationActivityModeling\PaperData\Figure1\sim_output_16_55.mat')
currs=sim_output.i_mem_matrix.*sim_output.cell_data.areas';
dipoles=currs*sim_output.cell_data.C./100;
dipoles=dipoles-dipoles(1,:);
maxTime=find(max(abs(dipoles(:,3)))==abs(dipoles(:,3)));
MaxTimeCurrs=currs(maxTime,:)-currs(1,:);
dists=sqrt(abs(dipoles(maxTime,:)*dirs)./(4*pi*0.2*100e6));
sinage=dipoles(maxTime,:)*dirs;
center=(sim_output.cell_data.C(MaxTimeCurrs>0,:)'*MaxTimeCurrs(MaxTimeCurrs>0)'./sum(MaxTimeCurrs(MaxTimeCurrs>0))+sim_output.cell_data.C(MaxTimeCurrs<0,:)'*(MaxTimeCurrs(MaxTimeCurrs<0)'./sum(MaxTimeCurrs(MaxTimeCurrs<0))))/2;
Iso=center+dirs.*dists.*1e6;
% figure
hold on
colors=parula(11);
plot3(Iso(1,sinage>0),Iso(2,sinage>0),Iso(3,sinage>0),'-','Color',colors(9,:),'LineWidth',2)
plot3(Iso(1,sinage<0),Iso(2,sinage<0),Iso(3,sinage<0),'-','Color',colors(3,:),'LineWidth',2)
dists=sqrt(abs(dipoles(maxTime,:)*dirs)./(4*pi*0.2*10e6));
sinage=dipoles(maxTime,:)*dirs;
center=(sim_output.cell_data.C(MaxTimeCurrs>0,:)'*MaxTimeCurrs(MaxTimeCurrs>0)'./sum(MaxTimeCurrs(MaxTimeCurrs>0))+sim_output.cell_data.C(MaxTimeCurrs<0,:)'*(MaxTimeCurrs(MaxTimeCurrs<0)'./sum(MaxTimeCurrs(MaxTimeCurrs<0))))/2;
Iso=center+dirs.*dists.*1e6;

plot3(Iso(1,sinage>0),Iso(2,sinage>0),Iso(3,sinage>0),'-','Color',colors(8,:),'LineWidth',2)
plot3(Iso(1,sinage<0),Iso(2,sinage<0),Iso(3,sinage<0),'-','Color',colors(4,:),'LineWidth',2)
print('Figures/NeuronWithIsosurfacesTerminalPSC.pdf','-dpdf','-painters')
%% AP
load('D:\PopulationActivityModeling\PaperData\Figure1\maxH_currinjection.mat')
currs=data_all{16}.i_mem_matrix.*data_all{16}.areas';
dipoles=currs*data_all{16}.coordinates./100;
dipoles=dipoles-dipoles(1,:);
maxTime=find(max(abs(dipoles(:,3)))==abs(dipoles(:,3)));
MaxTimeCurrs=currs(maxTime,:)-currs(1,:);
dists=sqrt(abs(dipoles(maxTime,:)*dirs)./(4*pi*0.2*10000e6));
sinage=dipoles(maxTime,:)*dirs;
center=(data_all{16}.coordinates(MaxTimeCurrs>0,:)'*MaxTimeCurrs(MaxTimeCurrs>0)'./sum(MaxTimeCurrs(MaxTimeCurrs>0))+data_all{16}.coordinates(MaxTimeCurrs<0,:)'*(MaxTimeCurrs(MaxTimeCurrs<0)'./sum(MaxTimeCurrs(MaxTimeCurrs<0))))/2;
Iso=center+dirs.*dists.*1e6;
% figure
figure
plotCellLines('cell_data',cell_data,'lw',lw); 
hold on
plot3(Iso(1,sinage>0),Iso(2,sinage>0),Iso(3,sinage>0),'-','Color',colors(11,:),'LineWidth',2)
plot3(Iso(1,sinage<0),Iso(2,sinage<0),Iso(3,sinage<0),'-','Color',colors(1,:),'LineWidth',2)
dists=sqrt(abs(dipoles(maxTime,:)*dirs)./(4*pi*0.2*1000e6));
sinage=dipoles(maxTime,:)*dirs;
center=(data_all{16}.coordinates(MaxTimeCurrs>0,:)'*MaxTimeCurrs(MaxTimeCurrs>0)'./sum(MaxTimeCurrs(MaxTimeCurrs>0))+data_all{16}.coordinates(MaxTimeCurrs<0,:)'*(MaxTimeCurrs(MaxTimeCurrs<0)'./sum(MaxTimeCurrs(MaxTimeCurrs<0))))/2;
Iso=center+dirs.*dists.*1e6;

plot3(Iso(1,sinage>0),Iso(2,sinage>0),Iso(3,sinage>0),'-','Color',colors(10,:),'LineWidth',2)
plot3(Iso(1,sinage<0),Iso(2,sinage<0),Iso(3,sinage<0),'-','Color',colors(2,:),'LineWidth',2)
print('Figures/NeuronWithIsosurfacesTerminalAP.pdf','-dpdf','-painters')
%% Terminal
loc=[927.926 -565.9513 -819.0567];
dipoles=[-0.142308438320205,0.0868298042284409,0.170388169398112];
% currs=data_all{16}.i_mem_matrix.*data_all{16}.areas';
% dipoles=currs*data_all{16}.coordinates./100;
% dipoles=dipoles-dipoles(1,:);
% maxTime=find(max(abs(dipoles(:,3)))==abs(dipoles(:,3)));
% MaxTimeCurrs=currs(maxTime,:)-currs(1,:);
dists=sqrt(abs(dipoles*dirs)./(4*pi*0.2*1e6));
sinage=dipoles*dirs;
center=loc';
Iso=center+dirs.*dists.*1e6;
% figure
figure
plotCellLines('cell_data',cell_data,'lw',lw); 
hold on
plot3(Iso(1,sinage>0),Iso(2,sinage>0),Iso(3,sinage>0),'-','Color',colors(7,:),'LineWidth',2)
plot3(Iso(1,sinage<0),Iso(2,sinage<0),Iso(3,sinage<0),'-','Color',colors(5,:),'LineWidth',2)

dists=sqrt(abs(dipoles*dirs)./(4*pi*0.2*10e6));
sinage=dipoles*dirs;
center=loc';
Iso=center+dirs.*dists.*1e6;

plot3(Iso(1,sinage>0),Iso(2,sinage>0),Iso(3,sinage>0),'-','Color',colors(8,:),'LineWidth',2)
plot3(Iso(1,sinage<0),Iso(2,sinage<0),Iso(3,sinage<0),'-','Color',colors(4,:),'LineWidth',2)

% dists=sqrt(abs(dipoles*dirs)./(4*pi*0.2*100e6));
% sinage=dipoles*dirs;
% center=loc';
% Iso=center+dirs.*dists.*1e6;
% 
% plot3(Iso(1,sinage>0),Iso(2,sinage>0),Iso(3,sinage>0),'-','Color',colors(9,:),'LineWidth',2)
% plot3(Iso(1,sinage<0),Iso(2,sinage<0),Iso(3,sinage<0),'-','Color',colors(3,:),'LineWidth',2)

print('Figures/NeuronWithIsosurfacesTerminal.pdf','-dpdf','-painters')
%%
% figure
% surf(peaks)
% colorbar
% caxis([-5 5])
