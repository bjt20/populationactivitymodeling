%% Fits
Folder = {'SynapticActivation','CurrentInj'};
AxonName = {'maxH_rax1','umaxH','maxH'};
activationName = {'synapact','currinjection'};
Axons={'NoAxon','Unmyel','Myel'};
tic
terminalCurrents={};
terms=[];
assInd={};
for k = 2%1:2 % activation type
    for j = 3 % axon types
                load(['PaperData\Figure5\' AxonName{j} '_' activationName{k} '.mat'])
        for i=16:20
            ct=1;
            current=(data_all{i}.i_mem_matrix.*data_all{i}.areas');
            %             current(:,1)=current(:,1)-data_all{i}.i_mem_matrix*data_all{i}.areas;
            current=current.*1e-2;%nA
            %% find sets of axon terminals
            load(['PaperData\Figure5\parent_inds' num2str(i) '.mat'])
            load(['PaperData\Figure5\sectypes' num2str(i) '.mat'])%2 or 5 are children with terminals
            load(['PaperData\Figure5\secnames' num2str(i) '.mat'])% Unmyelin is probs terminals
            for p=1:length(sectypes)
                if (contains(secnames{p},'Unmyelin') | contains(secnames{p},'Myelin') | contains(secnames{p},'Node')) & (sectypes(p)==2 | sectypes(p)==5)% check if terminal
                    [~,time]=max(abs(current(:,p)));
                    plusminus=current(time,p);
                    times(i-15,ct)=time;
                    start=plusminus;
                    ind=p;
                    terms=[terms;data_all{i}.coordinates(ind,:)];
                    terminalCurrents{i-15,ct}=current(time,ind);
                    assInd{i-15,ct}=ind;
                    while sign(plusminus)==sign(start) & ind ~= 1
                        terminalCurrents{i-15,ct}=[terminalCurrents{i-15,ct} current(time,parent_inds(ind-1))];
                        assInd{i-15,ct}=[assInd{i-15,ct} parent_inds(ind-1)];
                        plusminus = plusminus + current(time,parent_inds(ind-1));
                        ind=parent_inds(ind-1);
                    end
                    ct=ct+1;

                end
            end
        end
    end
end
%% Find dipole moments
for k=16:20
    coords=data_all{k}.coordinates;
    dipole=zeros(max(size(assInd)),3);
    loc=[0 0 0];
    for i=1:max(size(assInd))
        currSum=0;
        if ~isempty(terminalCurrents{k-15,i})
            si=sign(terminalCurrents{k-15,i}(1));%Start sign
            for j=1:length(assInd{k-15,i})
                currSum=currSum+terminalCurrents{k-15,i}(j);
                if sign(currSum)~=si
                    curr=terminalCurrents{k-15,i}(j)-currSum;
                    dipole(i,1)=dipole(i,1)+curr.*(coords(assInd{k-15,i}(j),1)-coords(assInd{k-15,i}(1),1))./1000;%nA*um*1000pA/nA*1m/1e6um=*1000pAm
                    
                    dipole(i,2)=dipole(i,2)+curr.*(coords(assInd{k-15,i}(j),2)-coords(assInd{k-15,i}(1),2))./1000;%nA*um*1000pA/nA*1m/1e6um=*1000pAm
                    dipole(i,3)=dipole(i,3)+curr.*(coords(assInd{k-15,i}(j),3)-coords(assInd{k-15,i}(1),3))./1000;%nA*um*1000pA/nA*1m/1e6um=*1000pAm
                else
                    dipole(i,1)=dipole(i,1)+terminalCurrents{k-15,i}(j).*(coords(assInd{k-15,i}(j),1)-coords(assInd{k-15,i}(1),1))./1000;%nA*um*1000pA/nA*1m/1e6um=*1000pAm
                    %             dipole(i,:)
                    %             pause
                    dipole(i,2)=dipole(i,2)+terminalCurrents{k-15,i}(j).*(coords(assInd{k-15,i}(j),2)-coords(assInd{k-15,i}(1),2))./1000;%nA*um*1000pA/nA*1m/1e6um=*1000pAm
                    dipole(i,3)=dipole(i,3)+terminalCurrents{k-15,i}(j).*(coords(assInd{k-15,i}(j),3)-coords(assInd{k-15,i}(1),3))./1000;%nA*um*1000pA/nA*1m/1e6um=*1000pAm
                end
                if assInd{k-15,i}(j)==1
                    dipole(i,1)=0;% can't go past soma, so we give up
                    dipole(i,2)=0;% can't go past soma, so we give up
                    dipole(i,3)=0;% can't go past soma, so we give up
                end
            end
        end
    end
    saveTerminals{k-15}=dipole;
    dipoleMags{k-15}=sqrt(sum(dipole.^2,2));
end

% load D:\CorticalNeuronDipole\plot_cell_data\cell_data\maxH\branchorders\branchorders19.mat
%% Plot Dipole Mags
figure
hold on
for i=1:5
    figure
    data=dipoleMags{i};
    data=data(data>0);
%     plot(linspace(i,i,length(data)),data,'*')
    boxplot(data)
    axis([0 2 0 0.2])
    fileName=['Figures\TerminalMomentsWithApMag_' num2str(i) '.pdf'];
    print(gcf,fileName,'-bestfit','-dpdf');
end
plot([1 2 3 4 5],[2.5373 3.0042 2.5933 2.5006 2.2309],'k*')
axis([0.5 5.5 0 3.1])
xticks([1 2 3 4 5])
ylabel('Dipole Moment (pA-m)')
xlabel('Layer 5 PC Clone')
title('Dipole Moments for All Terminals')
% print -dpng Figures\TerminalMomentsWithApMag.png
fileName='Figures\TerminalMomentsWithApMag.pdf';
% print(gcf,fileName,'-bestfit','-dpdf');

%% Isolate Terminals No reup
OnlyDipole=dipoleMags;
kick=[];
for k=1:5
    for j=1:103
        which=[];
        load(['PaperData\Figure5\secnames' num2str(k+15) '.mat'])% Unmyelin is probs terminals
        for i=1:length(assInd{k,j})
            if contains(secnames{assInd{k,j}(i)},'Unmyelin') | contains(secnames{assInd{k,j}(i)},'Node')
                which = [which i];
            end
        end
        a=terminalCurrents{k,j};
        b=a(which);
        c=diff(b>0);
        if sum(abs(c))>1
            OnlyDipole{k}(j)=0;
            kick=[kick;k j];
        end
    end
end
%% Plot Dipole Mags just Terminals
figure
hold on
for i=1:5
    figure
    data=OnlyDipole{i};
    data=data(data>0);
%     plot(linspace(i,i,length(data)),data,'*')
    boxplot(data)
    axis([0 2 0 0.2])
    fileName=['Figures\TerminalMomentsWithApMag_' num2str(i) '.pdf'];
%     print(gcf,fileName,'-bestfit','-dpdf');
    
%     plot(linspace(i,i,length(data)),data,'*')
end
plot([1 2 3 4 5],[2.5373 3.0042 2.5933 2.5006 2.2309],'k*')
axis([0.5 5.5 0 3.1])
xticks([1 2 3 4 5])
ylabel('Dipole Moment (pA-m)')
xlabel('Layer 5 PC Clone')
title('Dipole Moments for All Terminals')
% print -dpng Figures\TerminalMomentsNoReupWithApMag.png
fileName='Figures\TerminalMomentsNoReupWithApMag.pdf';
% print(gcf,fileName,'-bestfit','-dpdf');

figure
hold on
for i=1:5
    data=OnlyDipole{i};
    data=data(data>0);
    plot(linspace(i,i,length(data)),data,'*')
end
% plot([1 2 3 4 5],[2.5373 3.0042 2.5933 2.5006 2.2309],'k*')
axis([0.5 5.5 0 0.3])
xticks([1 2 3 4 5])
ylabel('Dipole Moment (pA-m)')
xlabel('Layer 5 PC Clone')
title('Dipole Moments for All Terminals')
% print -dpng Figures\TerminalMomentsNoReup.png
fileName='Figures\TerminalMomentsNoReup.pdf';
% print(gcf,fileName,'-bestfit','-dpdf');
%% Terminal Lengths vs Dipole Moment
for k=1:5
    coords=data_all{k+15}.coordinates;
    distances=zeros(1,max(size(assInd)));
    for i=1:max(size(assInd))
        for j=1:length(assInd{k,i})-1
            distances(i)=distances(i)+sqrt(sum((coords(assInd{k,i}(j+1),:)-coords(assInd{k,i}(j),:)).^2));
        end
    end
    lengths{k}=distances;
end
% for k=1:5
%     for j=1:103
%         if OnlyDipole{k}(j)==0
%             lengths{k}(j)=0;
%         end
%     end
% end
figure
hold on
for k=1:5   
    plot(lengths{k}(OnlyDipole{k}>0),OnlyDipole{k}(OnlyDipole{k}>0),'*')
end
xlabel('Terminal Length (\mum)')
ylabel('Dipole Moment (pA-m)')
title('Some Very Large Active Ends Exist')
% print -dpng Figures\TerminalLengthsvsMoment.png
fileName='Figures\TerminalLengthsvsMoment.pdf';
% print(gcf,fileName,'-bestfit','-dpdf');

%% Distribution of dipole moments
moments=[];
for k=1:5
    moments=[moments;OnlyDipole{k}(OnlyDipole{k}>0)];
end
figure
histogram(moments)
xlabel('Dipole Moment (pA-m)')
ylabel('Count')
title('Distribution of Terminal Dipole Moments')
% print -dpng Figures\HistogramDipoleMoments.png
figure
histogram(log10(moments))
xlabel('Log Transformed Dipole Moment (log(pA-m))')
ylabel('Count')
title('Distribution of Log Transformed Terminal Dipole Moments')
% print -dpng Figures\HistogramDipoleMoments_Log.png
fileName='Figures\HistogramDipoleMoments_Log.pdf';
% print(gcf,fileName,'-bestfit','-dpdf');
%% Terminal Dipole Directions
directions=[];
figure
hold on
for k=1:5
    for i=1:103
        if sum(saveTerminals{k}(i,:))~=0
            direction=saveTerminals{k}(i,:)./sqrt(sum(saveTerminals{k}(i,:).^2));
            plot3([0 direction(1)],[0 direction(2)],[0 direction(3)])
            directions=[directions;direction];
        end
    end
end
% print -dpng Figures\TerminalDipoleDirections.png
fileName='Figures\TerminalDipoleDirections.pdf';
% print(gcf,fileName,'-bestfit','-dpdf');
%% How many Active Terminals Are Enough?
num=[1 2 5 10 20 50 100 200 500 1000 2000 5000 10000 20000];
clear ManyTerminalDipole
for i=1:10000 % Trials
    for j=1:length(num)
        RandDirections=rand(num(j),3)-0.5;
        RandDirections=RandDirections./sqrt(sum(RandDirections.^2,2));
        mag=normrnd(-2.6309,0.6605,[num(j),1]);
        ScaledDipole=RandDirections.*10.^mag;
        if j>1
            SingleDipole=sum(ScaledDipole);
        else
            SingleDipole=ScaledDipole;
        end
        X(i,j)=SingleDipole(1);
        Y(i,j)=SingleDipole(2);
        Z(i,j)=SingleDipole(3);
        SingleDipoleMag=sqrt(sum(SingleDipole.^2));
        ManyTerminalDipole(i,j)=SingleDipoleMag;
    end
end
figure
histogram(mag)
xlabel('Log Transformed Dipole Moment (log(pA-m))')
ylabel('Count')
title('Simulated Distribution of Log Transformed Terminal Dipole Moments')
% print -dpng Figures\SimulatedHistogramDipoleMoments_Log.png
fileName='Figures\SimulatedHistogramDipoleMoments_Log.pdf';
% print(gcf,fileName,'-bestfit','-dpdf');
figure
errorbar(num,mean(ManyTerminalDipole),std(ManyTerminalDipole),'LineWidth',2)
hold on
plot([1 20000],[2.5733 2.5733],'k-','LineWidth',2)
% set(gca, 'YScale', 'log')
set(gca, 'XScale', 'log')
xlabel('Number of Terminals')
ylabel('Dipole Moment (pA-m)')
title('Number of Terminals Needed to Generate Similar Sized Signal as AP')
axis([1 21000 0 4.5])
% print -dpng Figures\NumberOfTerminalsNotLog.png
fileName='Figures\NumberOfTerminalsNotLog.pdf';
% print(gcf,fileName,'-bestfit','-dpdf');
%% Angle distribution
thetas=acosd(Z./sqrt(X.^2+Y.^2+Z.^2));
figure
histogram(thetas(:,end))
xlabel('Angle (Degrees)')
ylabel('Count')
fileName='Figures\AngleHistogram.pdf';
% print(gcf,fileName,'-bestfit','-dpdf');

%% Distribution of dipole magnitude and dipole in each direction
figure
histogram(X(:,end))
xlabel('Dipole Moment (pA-m)')
ylabel('Count')
title('Distribution of Dipole Moments X Direction')
% print -dpng Figures\DipoleMomentHistogramX.png

figure
histogram(Y(:,end))
xlabel('Dipole Moment (pA-m)')
ylabel('Count')
title('Distribution of Dipole Moments Y Direction')
% print -dpng Figures\DipoleMomentHistogramY.png

figure
histogram(Z(:,end))
xlabel('Dipole Moment (pA-m)')
ylabel('Count')
title('Distribution of Dipole Moments Z Direction')
% print -dpng Figures\DipoleMomentHistogramZ.png

figure
histogram(ManyTerminalDipole(:,end))
xlabel('Dipole Moment (pA-m)')
ylabel('Count')
title('Distribution of Dipole Moments Magnitude')
% print -dpng Figures\DipoleMomentHistogramMag.png
%% Dipole moment vs N with combined variances
figure
errorbar(num,mean(ManyTerminalDipole),std(ManyTerminalDipole),'LineWidth',2)
hold on
plot(num,[0.0150    0.0264    0.0416    0.0615    0.1099    0.1677    0.2255    0.3235    0.5243    0.7584    1.1193    1.6234    2.4032    3.3489],'r*')
% set(gca, 'YScale', 'log')
set(gca, 'XScale', 'log')
xlabel('Number of Terminals')
ylabel('Dipole Moment (pA-m)')
title('Number of Terminals Needed to Generate Similar Sized Signal as AP')
axis([1 21000 0 4.5])
legend('Simulated Moments','sqrt sum Variances')
% print -dpng Figures\SummedVariances.png
fileName='Figures\SummedVariances.pdf';
% print(gcf,fileName,'-bestfit','-dpdf');
%% Variance as a function of N
figure
loglog(num,std(X).^2,'k*')

xlabel('Number of Terminals')
ylabel('Variance of Dipole Moment Density (pA-m)')
title('Variance of Dipole Moment Density as a function of N')
% print -dpng Figures\VarianceAsFunctionOfN.png
fileName='Figures\VarianceAsFunctionOfN.pdf';
% print(gcf,fileName,'-bestfit','-dpdf');
%% Whole neuron
dipole=zeros(4001,3);
currents=current;
currents(:,1)=currents(:,1)-sum(currents,2);
voltage=[];
for i=1:4001
    dipole(i,1)=currents(i,:)*coords(:,1)/1000;
    dipole(i,2)=currents(i,:)*coords(:,2)/1000;
    dipole(i,3)=currents(i,:)*coords(:,3)/1000;
end
load PaperData\Figure5\DipoleMomentsAndDipolynessAPBestDirection_l5.mat
figure
plot(linspace(0,5,201),sqrt(sum(dipole(10:210,:).^2,2)),'LineWidth',2)
hold on
plot(linspace(0,5,201),abs(saveMoments{1,3,16,11}(100:300)),'LineWidth',2)
ylabel('|Dipole Moment|')
xlabel('Time (ms)')
title('Different Methods to Quantify Dipole Moment')
legend('Weighted Spatial Sum','Correlation')
% print -dpng Figures\DifferentWaysToQuantifyDipole.png


%%

for p=1:103
    inds(p)=assInd{p}(1);
    indLength(p)=length(terminalCurrents{p});
    tc(p)=terminalCurrents{p}(1);
    maxtc(p)=max(terminalCurrents{p});
end

