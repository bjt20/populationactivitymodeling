# PopulationActivityModeling

Code and data for Thio and Grill 2023 NeuroImage "Relative Contributions of Different Neural Sources to the EEG"

Code for main text figures is labeled by figure number.
Data for main text figures are in the PaperData folder.

Models are from https://senselab.med.yale.edu/modeldb/ Accession No. 241165.

