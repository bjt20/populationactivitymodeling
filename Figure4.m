load PaperData\Figure4\DipoleMags.mat
ct=1;
averages=cell(5,5);
windowSizes=1:10:4000;
for i=1:5
    for j=1:5
        for k=1:length(windowSizes)
            averages{i,j}=[averages{i,j} mean(dipoles{ct}(1:windowSizes(k),3)-dipoles{ct}(1,3)).*1000];%fA-m
        end
        ct=ct+1;
    end
end
%%
load PaperData\Figure4\PopulationNeuronsDipoleMoments.mat
windows=[2.5 5 10 20 50 90];
ns=[1 2 5 10 20 50 100 200 500 1000];
slopes=[];
for i=1:5
    figure
    hold on
    for j=1:length(windows)
        data=squeeze(SingleDipoleZ(j,:,:,i));
        P = polyfit(ns,mean(data),1);
        slopes(i,j)=P(1);
        plot(ns,mean(data))
        
    end
    legend('3 ms','5 ms','10 ms','20 ms','50 ms','90 ms')
    xlabel('Number of Neurons')
    ylabel('Dipole Moment (pA-m)')
    fileName=['Figures\DipoleMomentVsNumNeurons_' num2str(i) '.pdf'];
%     print(gcf,fileName,'-bestfit','-dpdf');
end
%%
colors=parula(5);
figure
% hold on
for i=1:5
    figure
    hold on
    plot(windowSizes./40,mean(vertcat(averages{i,:})),'Color',colors(i,:),'LineWidth',2)
    maxvals=max(vertcat(averages{i,:}));
    minvals=min(vertcat(averages{i,:}));
    patch([windowSizes./40 windowSizes(end:-1:1)./40 windowSizes(1)./40],[maxvals minvals(end:-1:1) maxvals(1)],colors(i,:),'FaceAlpha',0.1)
    plot(windows,slopes(i,:).*1000,'k*')
    axis([0 90 -220 520])
    xlabel('Window (ms)')
    ylabel('Dipole Moment (fA-m)')
    fileName=['Figures\DipoleMomentVsWindowSize_' num2str(i) '.pdf'];
%         print(gcf,fileName,'-bestfit','-dpdf');
end

% h=boxplot(abs(averages)','whisker',100)
%%
colors=parula(5);
figure
% hold on
ct=1;
figure

for i=1:5
   
    for j=1:5
        data=(dipoles{ct}(:,3)-dipoles{ct}(1,3)).*1000;
        subplot(5,5,ct)
        plot(linspace(0,100,4001),mean(data,2),'k','LineWidth',2)
        ct=ct+1;
        xlabel('Window (ms)')
        ylabel('Dipole Moment (fA-m)')
        axis([0 20 -1000 3200])
    end    
    
    fileName=['Figures\DipoleMomentTracesAll.pdf'];
%         print(gcf,fileName,'-bestfit','-dpdf');
end

% h=boxplot(abs(averages)','whisker',100)


%%
colors=parula(5);
figure
% hold on
for i=1:5
    figure
    hold on
    plot(windowSizes./40,mean(abs(100000000./vertcat(averages{i,:}))),'Color',colors(i,:),'LineWidth',2)
    maxvals=max(abs(100000000./vertcat(averages{i,:})));
    minvals=min(abs(100000000./vertcat(averages{i,:})));
    patch([windowSizes(2:end)./40 windowSizes(end:-1:2)./40],[maxvals(2:end) minvals(end:-1:2)],colors(i,:),'FaceAlpha',0.1)
%     plot(windows,slopes(i,:).*1000,'k*')
    axis([0 90 1e5 1e9])
    xlabel('Window (ms)')
    set(gca, 'YScale', 'log')
    ylabel('Dipole Moment (fA-m)')
    fileName=['Figures\NumNeuronsTo100nAmVsWindowSize_' num2str(i) '.pdf'];
%         print(gcf,fileName,'-bestfit','-dpdf');
end
